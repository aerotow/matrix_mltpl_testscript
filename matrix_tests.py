import random
import numpy as np
import os
import difflib
import sys
'''
Prereqs:
    Your Script has to accept the following inputs and outputs:
    
    Input:
    ----------start
    rA cA cB
    [Matrix A]
    \n
    [Matrix B]
    ----------end
    where
    rA: row count of matrix A
    cA: column count of matrix A
    rB: row count of matrix B
    cB: column count of matrix B
    [Matrix A]: matrix elements seperated by spaces of dim rA x cA
    [Matrix B]: matrix elements seperated by spaces of dim rB x cB
    
    Output:
    ----start
    [matrix C]
    \n
    time_in_miliseconds
    ----end
    
    where
    [matrix C]: matrix elements seperated by spaces of dim rA x cB
    time_in_miliseconds: your script should give the runtime of the current multiplication
Usage:
    python matrix_tests.py num_of_processes num_of_rounds num_of_iterations
        num_of_processes = number of processes in the mpirun call
        num_of_rounds = starting with two 2x2 matrices, each round size will be squared
        num_of_iterations = repetitions of each round, mean will be created
    e.g.
    python matrix_tests.py 4 4 5
<<<Confused?>>>
    Run the script once to have some random input files generated that you can inspect.
'''
# force same random output for comparison
np.random.seed(42)

procs = int(sys.argv[1])
rounds = int(sys.argv[2])
iterations = int(sys.argv[3])

for round in range(0, rounds):
    timings = []
    mat_dim = [2 ** round+1, 2 ** round+1, 2 ** round+1]
    for iteration in range(iterations):
        runCmd = "cat input.txt | mpirun -n " + str(procs) + " mm_mltply > output.txt"

        MatrixA = np.random.randint(100, size=(mat_dim[0], mat_dim[1]))
        MatrixB = np.random.randint(100, size=(mat_dim[1], mat_dim[2]))
        res = np.dot(MatrixA,MatrixB)

        with open("input.txt", "w") as inFile:
            inFile.write(str(mat_dim[0]) + " " + str(mat_dim[1])+ " " + str(mat_dim[2]) + "\n")
            for row in MatrixA:
                inFile.write(' '.join([str(col) for col in row]))
                inFile.write("\n")

            inFile.write("\n")
            for row in MatrixB:
                inFile.write(' '.join([str(col) for col in row]))
                inFile.write("\n")
        os.system(runCmd)
        with open("correctOutput.txt", "w") as correctOutFile:
            for row in res:
                correctOutFile.write(' '.join([str(col) for col in row]))
                correctOutFile.write("\n")
        with open('correctOutput.txt') as correctOutFile:
            MatrixC = [line.split() for line in correctOutFile.readlines()]
        with open('output.txt') as o:
            outMat = []
            timingLine = False
            for line in o:
                if line != "\n" and not timingLine:
                    outMat.append(line.split())
                elif not timingLine:
                    timingLine = True
                else: # end of Matrix, now timing output
                    timings.append(int(line))

        for i in range(mat_dim[0]):
            for j in range(mat_dim[2]):
                if int(MatrixC[i][j]) != int(float(outMat[i][j])):
                    print("Didn't work, check the input/output files")
                    break
        print("    Worked for iteration %d of round %d. (dimension = %d)" %(iteration, round, mat_dim[0]))
    if (len(timings) > 0):
        mean = sum(timings)/len(timings)
        print("Round %d took %d ms on average. (dimension = %d)"%(round, mean, mat_dim[0]))
