
## Prereqs

Your Script has to accept the following inputs and outputs:

```
rA cA cB
[Matrix A]
\n
[Matrix B]
```

#### where
```
rA: row count of matrix A
cA: column count of matrix A
rB: row count of matrix B
cB: column count of matrix B
[Matrix A]: matrix elements seperated by spaces of dim rA x cA
[Matrix B]: matrix elements seperated by spaces of dim rB x cB
```
### Output:

```
[matrix C]
\n
time_in_miliseconds
```
#### where

[matrix C]: matrix elements seperated by spaces of dim rA x cB
time_in_miliseconds: your script should give the runtime of the current multiplication

### Usage:
```
python matrix_tests.py num_of_processes num_of_rounds num_of_iterations

num_of_processes = number of processes in the mpirun call
num_of_rounds = starting with two 2x2 matrices, each round size will be squared
num_of_iterations = repetitions of each round, mean will be created
```
e.g.
```
python matrix_tests.py 4 4 5
```
### Confused?
Run the script once to have some random input files generated that you can inspect.
